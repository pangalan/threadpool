package main;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

import static java.lang.Thread.sleep;

public class Main {

    private static Map<Long, Thread> scriptIdToThread;

    public static void main(String[] args) throws InterruptedException {
        scriptIdToThread = new HashMap<>();
        //создаем фабрику, которая всегда будет возвращать поток под ТУЗОМ
        ThreadFactory threadFactory = new PPRBThreadFactory(new PPRBThreadService(), "user-some-suffix");
        //определяем экзекьютера с этой фабрикой
        ExecutorService threadPoolExecutor = Executors.newCachedThreadPool(threadFactory);
        for (long i = 0; i < 5; i++) {
            //пихаем раннеров в пул
            ScriptRunnable scriptRunnable = new ScriptRunnable().init();
            scriptRunnable.setThreadId(i);
            scriptIdToThread.put(i, scriptRunnable);
            threadPoolExecutor.execute(scriptRunnable);
        }
        //интерраптим
        sleep(20);
        scriptIdToThread.get(0L).interrupt();
        sleep(50);
        scriptIdToThread.get(1L).interrupt();
        scriptIdToThread.get(2L).interrupt();
        sleep(100);
        scriptIdToThread.get(3L).interrupt();
        scriptIdToThread.get(4L).interrupt();

        threadPoolExecutor.shutdown();
    }

}
