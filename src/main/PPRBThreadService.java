package main;

/**
 * Заглушка для создания потока под ТУЗом
 */
public class PPRBThreadService {

    public Thread getThread(String userName, Runnable thread) {
        return new Thread(thread, userName);
    }

}
