package main;

import java.util.concurrent.ThreadFactory;

public class PPRBThreadFactory implements ThreadFactory {

    private PPRBThreadService service;
    private String userName;

    public PPRBThreadFactory(PPRBThreadService service, String userName) {
        this.service = service;
        this.userName = userName;
    }

    @Override
    public Thread newThread(Runnable r) {
        return service.getThread(userName, r);
    }
}
