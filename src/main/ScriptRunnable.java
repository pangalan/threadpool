package main;

import java.util.concurrent.atomic.AtomicBoolean;

public class ScriptRunnable extends Thread {

    private String someParameter;
    private AtomicBoolean isInterrupted;
    private long threadId;

    public ScriptRunnable init() {
        someParameter = "some value";
        threadId = Thread.currentThread().getId();
        isInterrupted = new AtomicBoolean(false);
        return this;
    }

    @Override
    public void run() {
        System.out.println("My id is " + threadId);
        System.out.println("I'm running script and my user is " + Thread.currentThread().getName());
        while (!isInterrupted.get()) {
            System.out.println(threadId);
        }
    }

    @Override
    public void interrupt() {
        System.out.println("I'm interrupted");
        isInterrupted.set(true);
    }

    public long getThreadId() {
        return threadId;
    }

    public void setThreadId(long threadId) {
        this.threadId = threadId;
    }
}
